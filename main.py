import argparse
import cv2
import argparse
import re 
import numpy as np 
import keras_segmentation

class BillOCR:
	def __init__(self,image_address,retrain=False,checkpoint='./models/resnet50_unet'):
		self.image_address = image_address
		self.width = 32*64
		self.height = 32*58
		self.retrain = retrain
		self.checkpoint = checkpoint
		self.model = keras_segmentation.models.unet.resnet50_unet(n_classes=3,  input_height=32*24, input_width=32*18,)

	def resize_image(image,width=32*18,height = 32*24, to_gray= False):	
		''' 
			Resize image without loosing aspect ration
			Args : 
				image(np.array) - image to be resized 
				width(int) - image output width . Default = 32*18
				height(int) - image output height. Default = 32*24
				to_gray(boolean) - convert image to gray. Default =  False 
			returns: Scaled vesion of the image of specifed output width and height 
		'''
		if to_gray:
			image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
	 	# Deal with width oriented scaling or height oriented scaling 
		if image.shape[1] < image.shape[0]:  
			new_height = height
			new_width  = new_height * image.shape[1] / image.shape[0]
		else: 
			new_width = width
			new_height  = new_width * image.shape[0] / image.shape[1]
		return cv2.resize(image, (int(new_width), int(new_height)))

	# Make the image size constant by padding 0's to the image 
	def pad_zeros(image,width=32*18,height = 32*24):
		''' 
			Padd image with zeros to compensate the output width and height
			Args : 
				image(np.array) - image to be resized 
				width(int) - image output width . Default = 32*18
				height(int) - image output height. Default = 32*24
			returns: zero padded vesion of the image of specifed output width and height 
		'''
		result = np.zeros((height,width,3))
		image = image[:heigth,:width]
		result[:image.shape[0],:image.shape[1]] = image
		return result

	def prepare_for_tesseract(img):	
		'''
			Resize image to greater height for Tesseract OCR 
			returns : Resized and padded imaeg 
		'''
		res = resize_image(img,width=32*58,height=32*64)
		padded_img = pad_zeros(res,width=32*58,height=32*64)
		return padded_img
	
	def get_details(self):
		'''
			Returns the k number at the moment.
		'''
		#Get predictions 
		img = cv2.imread(self.image_address)		
		op = self.model.predict_segmentation(img,checkpoints_path=self.checkpoint).astype('float32')
		
		op = cv2.cvtColor(op,cv2.COLOR_GRAY2BGR)
		# preapre image for tesseract
		img = prepare_for_tesseract(img)	
		op = prepare_for_tesseract(op)
		unmask = cv2.cvtColor(np.where(op==2,img,0).astype('uint8'),cv2.COLOR_BGR2GRAY)
		# Threshold		
		thresh = cv2.adaptiveThreshold(unmask, 105, cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY, 155, 28)
  
		return re.findall('\d{11}',pytesseract.image_to_string(thresh,config='digits'))


parser = argparse.ArgumentParser(description='OCR to extract k number from bill')
parser.add_argument('--image_address', type=str, help='address of the image')
parser.add_argument('--retrain', type=bool, help='True if model retraining is required else False, default=False')
parser.add_argument('--checkpoint',type=str,help='address of the checkpoint of the model')
args = parser.parse_args()

b = BillOCR(args.image_address)
print(b.image_address)
print(b.get_details())
